var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var EventEmitter = require('events').EventEmitter;

exports.connect = connect;
var connection = exports.connection = new EventEmitter();

function connect (location, callback) {
  var file = 'db.data';

  var exists = fs.existsSync(path.join(location, file));
  exports.location = path.join(location, file);

  // if (!exists) {
  //   fs.writeFile(exports.location, JSON.stringify({}), function (err) {
  //     if (err) {
  //       if (callback && _.isFunction(callback)) {
  //         callback(err);
  //       }
  //       return connection.emit('error', err);
  //     }

  //     if (callback && _.isFunction(callback)) {
  //       callback(null);
  //     }
  //     return connection.emit('open', exports);
  //   });
  // }

  // if (callback && _.isFunction(callback)) {
  //   callback(null);
  // }
  // connection.emit('open', exports);

  process.nextTick(function () {
    if (!exists) {
      try {
        fs.writeFileSync(exports.location, JSON.stringify({}));
      } catch (err) {
        if (callback && _.isFunction(callback)) {
          return callback(err);
        }
        return exports.connection.emit('error', err);
      }
    }

    if (callback && _.isFunction(callback)) {
      callback(null);
    }
    connection.emit('open', exports);
  });
}

function model(modelName) {
  var model = new Model(modelName);
  return model;
}

exports.model = model;

function Model(modelName) {
  this.name = modelName;
  this.dbname = modelName.toLowerCase();
  this.file = exports.location;
}

Model.prototype.getNextID = function(callback) {
    var _this = this;

    fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {
      if (err) return callback(err);

      try {
        data = JSON.parse(data)
      } catch(err) {
        return callback('JSON parse on getNextID: ' + err);
      }

      if (!data[_this.dbname]) {
        data[_this.dbname] = [];
        return callback(null, 0);
      } else {
        // if (data[_this.dbname].length === 0) {
        //   return callback(null, 0);
        // } else {
        //   var last = data[_this.dbname].length - 1;
        //   var nextId = data[_this.dbname][last]._id + 1;
        //   return callback(null, nextId);
        // }
        return callback(null, data[_this.dbname].length);
      }

    })
};

Model.prototype.insert = function(object, callback) {
  var _this = this;

  this.getNextID(function (err, id) {

    if (err) return callback(err);

    fs.readFile(_this.file, {encoding: 'utf8'}, function (err, data) {
      if (err) return callback(err);

      try {
        data = JSON.parse(data);
      } catch (err) {
        return callback('JSON.parse at insert: ' + err);
      }

      object['_id'] = id;

      if (!data[_this.dbname]) {
        data[_this.dbname] = [];
        data[_this.dbname].push(object);
      } else {
        data[_this.dbname].push(object);
      }

      fs.writeFile(_this.file, JSON.stringify(data), function (err) {
          if (err) return callback(err);

          return callback(null, object);
      }); 
    });
  });
};

Model.prototype.read = function(query, callback) {
  // body...

  if (_.isFunction(query)) {
    callback = query;
    this.getDocs(callback);
  } else {
    this.getDoc(query, callback);
  }
};

Model.prototype.getDocs = function(callback) {
  // body...

  var _this = this;
  fs.readFile(this.file, {encoding: 'utf8'}, function(err, data) {
    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch(err) {
      callback('JSON.parse at getDocs ' + err);
    }

    callback(null, data[_this.dbname]);
  });

};

Model.prototype.getDoc = function(query, callback) {
  // body...
  var _this = this;
  fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {

    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch(err) {
      return callback('JSON.parse at getDoc: ' + err);
    }

    var entry = _.findWhere(data[_this.dbname], query);

    callback(null, entry);
  })
};

Model.prototype.delete = function(query, callback) {
  
  var _this = this;

  fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {
    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch (err) {
      return callback(err);
    }

    _.each(data[_this.dbname], function (elem, index) {
      if (elem._id === query._id) {
        data[_this.dbname].splice(index, 1);
        return;
      }
    });

    fs.writeFile(_this.file, JSON.stringify(data), function (err) {
      if (err) return callback(err);

      callback(null);
    });
  });
};

Model.prototype.update = function(query, callback) {
  var _this = this;

  fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {
    var newEntry;

    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch (err) {
      return callback(err);
    }

    _.each(data[_this.dbname], function (elem, index) {
      if (elem._id === query._id) {
        newEntry = data[_this.dbname][index] = query;
        return;
      }
    });

    fs.writeFile(_this.file, JSON.stringify(data), function (err) {
      if (err) return callback(err);

      callback(null, newEntry);
    });
  });
};

Model.prototype.clear = function(callback) {
  var _this = this;

  fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {
    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch (err) {
      callback('JSON.parse on clear: ' + err);
    }

    if (data[_this.dbname]) {
      data[_this.dbname] = [];
    }

    fs.writeFile(_this.file, JSON.stringify(data), function (err) {
      if (err) return callback(err);
      callback(null);
    });
  });
};