var db = require('../lib/db2');
var assert = require('assert');

var fs = require('fs');

describe('Connection', function() {

	describe('#connect', function () {

		it('should fail connection to /usr/bin', function (done) {
			db.connect('/usr/bin', function (err) {
				if (err) return done();
				done(new Error('Did not fail on /usr/bin.'));
			});
		});

		it('should not fail connection on test/db', function (done) {
			db.connect('test/db', function (err) {
				if (err) return done(err);
				done();
			})
		});

	});
});

describe('Model', function() {
	var todo;

	before(function () {
		todo = db.model('Todo');
	});

	describe('#insert', function() {
		it('should create a document', function (done) {
			todo.insert({ entry: 'learn javascript' }, function (err, doc){
				if (err) return done(err);

				assert.equal(doc.entry, 'learn javascript');

				done();
			});
		});

		after(function (done) {
			todo.clear(done);
		});
	});

	describe('#read', function () {
		var entries = [];

		before(function (done) {
			todo.insert({ entry: 'learn javascript'}, function (err, doc) {
				if (err) return done(err);
				entries.push(doc);
				todo.insert({ entry: 'learn nodejs'}, function (err, doc) {
					if (err) return done(err);
					entries.push(doc);
					done();
				});
			});
		});

		it('should return an entry with id equals to the query id', function (done) {
			console.log({_id: entries[0]._id});
			todo.read({_id: entries[0]._id}, function (err, doc) {
				if (err) return done(err);

				assert.deepEqual(entries[0], doc);

				done();
			});
		});

		it('should also return all docs if without query parameter', function (done) {
			todo.read(function (err, docs) {
				if (err) return done(err);

				assert.deepEqual(entries, docs);

				done();
			});
		});

		after(function (done) {
			todo.clear(done);
		});
	});

	describe('#delete', function() {
		var id;

		before(function (done) {
			todo.insert({entry: 'learn nodejs'}, function (err, doc) {
				if (err) return done(err);
	
				id = doc._id;
				
				done();
			});
		});

		it('should delete the entry with id', function (done) {
			console.log(id)
			todo.delete({ _id: id }, function (err) {

				if (err) return done(err);

				todo.read({ _id: id }, function (err, doc) {
					if (err) return done(err);

					assert.equal(null, doc);

					done();
				});
			});
		});
	});

	describe('#update', function () {

		it('should update an entry previously inserted', function(done) {
			todo.insert({ entry: 'learn javascript' }, function (err, doc) {
				if (err) return done(err);

				var newEntry = { _id: doc._id, entry: 'javascript is awesome' };

				todo.update(newEntry, function (err, doc) {
					if (err) return done(err);

					todo.read({ _id: doc._id }, function (err, doc) {
						if (err) return done(err);

						assert.deepEqual(doc, newEntry);

						done();
					});
				});
			});
		});

	});

	describe('#id uniqueness', function () {
		var idToRemove;

		before(function (done) {
			console.log('       inserting 2 entries and deleting the first one');
			todo.insert({ entry: 'learn javascript'}, function (err, doc) {
				if (err) return done(err);

				idToRemove = doc._id;

				todo.insert({ entry: 'learn nodejs'}, function (err, doc) {
					if (err) return done(err);
					
					todo.delete({ _id: idToRemove }, function (err) {
						if (err) return done(err);

						done();
					});
				});
			});
		});

		it('should return the same entry that is inserted', function (done) {
			console.log('entry to insert', { entry: 'eat pizza' });

			todo.insert({ entry: 'eat pizza' }, function (err, newDoc) {
				if (err) return done(err);

				todo.read({ _id: newDoc._id }, function (err, doc) {
					if (err) return done(err);

					assert.deepEqual(doc, newDoc);
		
					done();
				});
			});
		});

	});

	after(function (done) {
		fs.unlink('test/db/db.data', done);
	});
});